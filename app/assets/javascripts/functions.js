Number.prototype.formatMoney = function(c, d, t){
var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
 };

function isNumberKey(evt) { 
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	return true;
}

function growl(text, timer) {
	$('#growl p').html(text);
	$('#growl').fadeIn();

	if (timer > 0) {
		setTimeout(function(){$("#growl").fadeOut();}, timer);		
	}
}

function countDeliveryOrderTotal(){
	var quantity = $('#deliveryorder_quantity').val().split('.').join('').replace(',','.');
	var unitprice = $('#deliveryorder_unit_price').val().split('.').join('').replace(',','.');

	if(quantity == "") bruto ="0";
	if(unitprice == "") tara = "0";

	var total = Number(quantity) * Number(unitprice);

	$('#deliveryorder_total').val(total.formatMoney(2,',','.'));
}

function countDeliveryNettSupplierpermit(){
	var bruto = $('#supplierpermit_delivery_weight_bruto').val().split('.').join('').replace(',','.');
	var tara = $('#supplierpermit_delivery_weight_tara').val().split('.').join('').replace(',','.');

	if(bruto == "") bruto ="0";
	if(tara == "") tara = "0";

	
	var nett = Number(bruto) - Number(tara);

	$('#supplierpermit_delivery_weight_nett').val(nett.formatMoney(0,',','.'));
	
}

function countArrivalNettSupplierpermit(){
	var bruto = $('#supplierpermit_arrival_weight_bruto').val().split('.').join('').replace(',','.');
	var tara = $('#supplierpermit_arrival_weight_tara').val().split('.').join('').replace(',','.');
	var unitprice = $('#supplierpermit_unit_price').html().split('.').join('').replace(',','.');

	if(bruto == "") bruto ="0";
	if(tara == "") tara = "0";

	var nett = Number(bruto) - Number(tara);

	$('#supplierpermit_arrival_weight_nett').val(nett.formatMoney(0,',','.'));
}

function countDeliveryNettPermit(){
	var bruto = $('#permit_delivery_weight_bruto').val().split('.').join('').replace(',','.');
	var tara = $('#permit_delivery_weight_tara').val().split('.').join('').replace(',','.');

	if(bruto == "") bruto ="0";
	if(tara == "") tara = "0";

	var nett = Number(bruto) - Number(tara);

	$('#permit_delivery_weight_nett').val(nett.formatMoney(0,',','.'));
}

function countInventoryNettPermit(){
	var bruto = "";
	var tara = $('#permit_inventory_weight_tara').val().split('.').join('').replace(',','.');
	
	var scen = $('#permit_scenario_type').val();

	switch(scen) {
		case '2':
			bruto = $('#permit_inventory_weight_bruto_after').val().split('.').join('').replace(',','.');
			break;
		case '3':
			bruto = $('#permit_inventory_weight_bruto_before').val().split('.').join('').replace(',','.');
			break;
		case '4':
			bruto = $('#permit_inventory_weight_bruto_after').val().split('.').join('').replace(',','.');
			break;
	}
	
	if(bruto == "") bruto ="0";
	if(tara == "") tara = "0";

	var nett = Number(bruto) - Number(tara);

	$('#permit_inventory_weight_nett').val(nett.formatMoney(0,',','.'));
}

function countArrivalNettPermit(){
	var bruto = $('#permit_arrival_weight_bruto').val().split('.').join('').replace(',','.');
	var tara = $('#permit_arrival_weight_tara').val().split('.').join('').replace(',','.');
	
	if(bruto == "") bruto ="0";
	if(tara == "") tara = "0";

	var nett = Number(bruto) - Number(tara);

	$('#permit_arrival_weight_nett').val(nett.formatMoney(0,',','.'));
}

function countReturNett(){
	var bruto = $('#retur_retur_weight_bruto').val().split('.').join('').replace(',','.');
	var tara = $('#retur_retur_weight_tara').val().split('.').join('').replace(',','.');
	
	if(bruto == "") bruto ="0";
	if(tara == "") tara = "0";

	var nett = Number(bruto) - Number(tara);

	$('#retur_retur_weight_nett').val(nett.formatMoney(0,',','.'));
}

function getWeightVehiclePermit()
{
	var vehicle_id = $('#permit_vehicle_id').val();

	if(vehicle_id != ''){
		$.ajax({
		type: "GET",
		url: "/permits/getweightvehicle/" + vehicle_id,
		success: function(data) {
			$('#permit_delivery_weight_tara').val(data.weight.formatMoney(0,',','.'));
			$('#permit_inventory_weight_tara').val(data.weight.formatMoney(0,',','.'));
			countDeliveryNettPermit();
		},
		failure: function() {alert("Error. Mohon refresh browser Anda.");}
	});	
	}
}

function checkboxInventoryPermit()
{
	$('#div_inventory').hide();
	
	if($('#cb_inventory').prop('checked')){
		$('#div_inventory').show();
	}
}

function changeScenarioTypePermit()
{
	$('#divDeliveryOrder').hide();
	$('#divPurchaseOrder').hide();
	$('#divPurchaseOrder').hide();
	$('#divSupplier').hide();
	$('#divDeliveryInventoryWeightBrutoBefore').hide();
	$('#divDeliveryInventoryWeightBrutoAfter').hide();
	$('#divDeliveryInventoryWeight').hide();
	$('#divArrivalWeight').hide();

	var scen = $('#permit_scenario_type').val();

	switch(scen) {
		case '1': 
			$('#divDeliveryOrder').show();
			$('#divPurchaseOrder').show();
			$('#divDeliveryWeight').show();	
			$('#divArrivalWeight').show();
			break;
		case '2':
			$('#divDeliveryOrder').show();
			$('#divPurchaseOrder').show();
			$('#divDeliveryWeight').show();
			$('#divDeliveryInventoryWeightBrutoBefore').show();
			$('#divDeliveryInventoryWeightBrutoAfter').show();
			$('#divDeliveryInventoryWeight').show();
			$('#divArrivalWeight').show();
			break;
		case '3':
			$('#divDeliveryOrder').show();
			$('#divDeliveryWeight').show();
			$('#divDeliveryInventoryWeightBrutoBefore').show();
			$('#divDeliveryInventoryWeight').show();
			break;
		case '4':
			$('#divPurchaseOrder').show();
			$('#divDeliveryInventoryWeightBrutoAfter').show();
			$('#divDeliveryInventoryWeight').show();
			$('#divArrivalWeight').show();
	}
	
}

function getSaleCurrencyPermit(){
	var purchaseorder_id = $('#permit_purchaseorder_id').val();

	if(purchaseorder_id != ''){
		$.ajax({
		type: "GET",
		url: "/permits/getsalecurrency/" + purchaseorder_id,
		success: function(data) {
			$('#permit_currency').val(data.currency);
		},
		failure: function() {alert("Error. Mohon refresh browser Anda.");}
	});	
	}
}

function getPurchaseorderitem(id){
	$.ajax({
		type: "GET",
		url: "/purchaseorders/getpurchaseorderitem/" + id,
		success: function(data) {
			$('#modal_purchaseorder_id').val(data.purchaseorder_id);
			$('#modal_purchaseorderitem_id').val(data.purchaseorderitem_id);
			$('#modal_product_name').val(data.product_name);
			$('#modal_specification').val(data.specification);
			$('#modal_quantity').val(Number(data.quantity).formatMoney(0,',','.'));
			$('#modal_unit_name').val(data.unit_name);
			$('#modal_unit_price').val(Number(data.unit_price).formatMoney(3,',','.'));
		},
		failure: function() {alert("Error. Mohon refresh browser Anda.");}
	});	
}

function countTotalPurchaseOrder(){
	var subtotal = $("#lblSubtotal").html().split('.').join('').replace(',','.');
	var discount = Number($("#purchaseorder_discount").val().split('.').join('').replace(',','.'));
	$("#lblDiscount").html(discount.formatMoney(0,',','.'));

	var additionalcharge = Number($("#purchaseorder_additional_charge").val().split('.').join('').replace(',','.'));
	$("#lblAdditionalCharge").html(additionalcharge.formatMoney(0,',','.'));

	var total = Number(subtotal) - discount + additionalcharge;

	var tax = 0;

	if($('#is_tax').prop('checked'))
	{
		tax = 0.1 * total;
		total = total + tax;
	}

	$("#purchaseorder_tax").val(tax);
	$("#lblTax").html(tax.formatMoney(0,',','.'));

	$("#purchaseorder_total").val(total);
	$("#lblTotal").html(total.formatMoney(0,',','.'));
}

function addTaxInvoice()
{
	var invoice_id = $('#invoice_id').val();
	var is_tax = false;

	if($('#is_tax').prop('checked')){
		is_tax = true
	}

	$.ajax({
		type: "POST",
		url: "/invoices/addtaxinvoice/" + invoice_id + "/" + is_tax,
		success: function(data) {
		},
		failure: function() {alert("Error. Mohon refresh browser Anda.");}
	});
}

function invoicepaymentFull()
{
	if($('#is_full').prop('checked')){
		$('#invoicepayment_total').val($('#unpaid_invoice').val());
	}
}

function invoiceChangeRate()
{
	var invoice_id = $('#invoice_id').val();
	var change_rate = $('#invoice_change_rate').val().split('.').join('').replace(',','.');;

	$.ajax({
		type: "POST",
		url: "/invoices/changerate/" + invoice_id + "/" + change_rate,
		success: function(data) {
		},
		failure: function() {alert("Error. Mohon refresh browser Anda.");}
	});
}

function checkInvoiceType() {
  if ($('#invoice_invoice_generic_true').is(':checked')) {
    $('.po-box').hide();
    // $('.text_currency').prop("readonly",false);
    $('#select-currency').prop("disabled",false)
  } else if ($('#invoice_invoice_generic_false').is(':checked')) {    
    $('.po-box').show();
    // $('.text_currency').prop("readonly",true);
    $('#select-currency').prop("disabled",true)


  }
}

function selectCurrency() {
    var x = document.getElementById("select-currency").value;
    if (x == 'USD')
	    $('.text_currency').prop("readonly",false);
	else
	    $('.text_currency').prop("readonly",true);
}

 function submitTwoForms() {
 	var form1 =  document.getElementById("filterform")
 	$.ajax({
           type: "GET",
           url: "/invoices/new/"+ $("#filterform").serialize()+ "" + $("#filterformInvoice").serialize(),
           data: $(this).parent().serialize(), // changed
           success: function(data)
           {
	        $("#filterform").submit();   //assuming id of second form  is form2
	        // $("#filterformInvoice").submit();   //assuming id of second form  is form2

               alert(data); // show response from the php script.
           }
         });
    return false; // avoid to execute the actual submit of the form.

// // var dataObject = {"filterformInvoice"};
//     $.ajax({
//       url: "",
//       data : "",
//       type : "GET",
//       success: function(){
//         $("#filterform").submit();   //assuming id of second form  is form2
//       }
//     });
//     return false;   //to prevent submit
//     // $('form').each(function() {
//     // var that = $(this);
//     // $.url(that.attr('action'), that.serialize());
// // });
 }

 function post_form_data(data) {
    $.ajax({
        type: 'POST',
        url: 'somewhere',
        data: data,
        success: function () {},
        error: function () {}
    });
}

$('button').on('click', function () {
    $('form').each(function () {
        post_form_data($(this).serialize());
    });
});

 function submitOne(){
 	document.getElementById("filterform").submit();
 	var a = $("#filterform").serialize()

 	var b = $("#filterformInvoice").serialize()
    document.getElementById("filterformInvoice").submit();

 	$('#filterformInvoice').get(0).setAttribute('action', '/invoices/new?'+a+'&'+b); //this works
    alert(""+a+""+b)
//  	$('#filterformInvoice').on('submit', function() {
//  		// alert($(this).find('input').val());
//     submitTwoForms();
//     return false;
// });
 	// $("#filterformInvoice").submit();
 	// window.submitTwoForms();
  //   return false; // avoid to execute the actual submit of the form.
 }


