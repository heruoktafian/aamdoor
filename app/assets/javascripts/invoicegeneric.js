function removeInvoiceGenericStock(el)
{
  var self = $(el), 
    container = self.closest('tr'); 

  $.ajax({
    url: self.attr('href'),
    type: 'POST',
    data: {'_method': 'delete'},
    success: function (response)
    {
      container.slideUp('slow', function() {container.remove();});
    }
  });

  return false;
}

$(function ()
{
	// Event listeners
	$(document).on('invoices:edit', function(event, delegated_response)
	{
		$('#invoicegenericitems').html(delegated_response.html);

    // $('#modal-form .fa').addClass(delegated_response.icon);

    $('#modal-form .info').html(delegated_response.message).show();

    setTimeout(function(){ 
      $('#modal-form .info').fadeOut(); }, 1500);
	});

	// Add stock
  $('#modal-form').submit(function (event)
  {
    event&&event.preventDefault();    

    var self = $(this);

    $.ajax({
      url: self.attr('action'),
      type: self.attr('method'),
      data: self.serialize(),
      success: function (response)
      {
        $(document).trigger('invoices:edit', response);
      }
    });
  }); 

});
