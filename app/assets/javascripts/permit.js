$(function ()
{
	if ($('#permit_submit').length > 0) {
		
		$('#permit_submit').on('click', function() {
			e.preventDefault();

			var errors = "";
			var form = $(this).closest("form");

			if ($('#cbInventory').prop('checked') && $('#inventory_weight_tara').val() == '') {
				errors = addComma(errors, "<strong>Tarra Ngoro</strong>");
			}

			if (errors == "") {
				form.submit();
			}
			else {
				errors = "Harap mengisi data:<br>" + errors;
				showMessageBox(errors, 2500);
			}
		});
	}

});