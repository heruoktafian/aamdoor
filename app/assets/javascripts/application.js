//= require jquery.min
//= require jquery_ujs
//= require_tree .

$(function ()
{
	$('.chzn-select').chosen({ width: '18em' });

	$('.ch-select').chosen({ width: '18em' });

	$('.tipsy').tooltipsy({delay : 0});

	$('.date-picker').datepicker({"dateFormat" : "dd-mm-yy", changeMonth : true, changeYear : true});

	$('.number-only').keypress(function(event) { return isNumberKey(event); });

	$(".currency-bank").maskMoney({thousands:'.', decimal:',', defaultZero:0});

	$(".currency-special").maskMoney({thousands:'.', decimal:',', defaultZero:0, precision:3});

	$(".currency-expense").maskMoney({thousands:'.', decimal:',', defaultZero:0, precision:0});

	$('.toggle-deleted').on('change', function() { $(this).is(':checked') ? $('tr.deleted').show() : $('tr.deleted').hide(); });

	if ($('#flash').length > 0) growl($('#flash').html(), 2500);

	$('#growl .close').on('click', function() { $('#growl').fadeOut(); });

	if ($('.tablesorterFilters').length > 0) {
		$(".tablesorter").tablesorter({
			cssInfoBlock : "tablesorter-no-sort",
			widgets: ['filter'],
			widgetOptions : {
				// filter_columnFilters : true
			}
		});
	}
	else {
		$(".tablesorter").tablesorter();
	}

	$('.lazy').lazyload({
		effect : "fadeIn"
	});

});

function formatCurrency (num) {
    return num
       .toFixed(2) // always two decimal digits
       .replace(".", ",") // replace decimal point character with ,
       .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.") // use . as a separator
}
