json.array!(@users) do |user|
  json.extract! user, :id, :username, :address
  json.url user_url(user, format: :json)
end
