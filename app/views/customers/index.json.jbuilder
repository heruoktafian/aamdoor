json.array!(@customers) do |customer|
  json.extract! customer, :id, :deleted, :enabled, :name, :contact, :email, :city, :postcode, :phone, :mobile, :fax, :npwp, :npwp_address, :paymentdays, :description
  json.url customer_url(customer, format: :json)
end
