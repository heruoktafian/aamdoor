json.extract! @customer, :id, :deleted, :enabled, :name, :contact, :email, :city, :postcode, :phone, :mobile, :fax, :npwp, :npwp_address, :paymentdays, :description, :created_at, :updated_at
