class ApplicationController < ActionController::Base
  before_filter :authenticate_user!
  layout :layout_by_resource
  helper_method :active_section, :active_node

  include ApplicationHelper
  protect_from_forgery

  def index
  	respond_to :html
  end
  protected
    # Root class and active menu items
    def active_section
      @active_section ||= params[:controller]
    end

    def active_node
      @active_node ||= params[:action]
    end

    # System methods
    def not_found
      raise ActionController::RoutingError.new "Not Found"
    end

    def layout_by_resource
      if devise_controller?
        "login"
      else
        "application"
      end
    end
end
