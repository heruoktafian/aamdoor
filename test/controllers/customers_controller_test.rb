require 'test_helper'

class CustomersControllerTest < ActionController::TestCase
  setup do
    @customer = customers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:customers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create customer" do
    assert_difference('Customer.count') do
      post :create, customer: { city: @customer.city, contact: @customer.contact, deleted: @customer.deleted, description: @customer.description, email: @customer.email, enabled: @customer.enabled, fax: @customer.fax, mobile: @customer.mobile, name: @customer.name, npwp: @customer.npwp, npwp_address: @customer.npwp_address, paymentdays: @customer.paymentdays, phone: @customer.phone, postcode: @customer.postcode }
    end

    assert_redirected_to customer_path(assigns(:customer))
  end

  test "should show customer" do
    get :show, id: @customer
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @customer
    assert_response :success
  end

  test "should update customer" do
    patch :update, id: @customer, customer: { city: @customer.city, contact: @customer.contact, deleted: @customer.deleted, description: @customer.description, email: @customer.email, enabled: @customer.enabled, fax: @customer.fax, mobile: @customer.mobile, name: @customer.name, npwp: @customer.npwp, npwp_address: @customer.npwp_address, paymentdays: @customer.paymentdays, phone: @customer.phone, postcode: @customer.postcode }
    assert_redirected_to customer_path(assigns(:customer))
  end

  test "should destroy customer" do
    assert_difference('Customer.count', -1) do
      delete :destroy, id: @customer
    end

    assert_redirected_to customers_path
  end
end
