require 'test_helper'

class KelilingControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get bulat" do
    get :bulat
    assert_response :success
  end

  test "should get square" do
    get :square
    assert_response :success
  end

end
