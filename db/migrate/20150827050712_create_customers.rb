class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.boolean :deleted
      t.boolean :enabled
      t.string :name
      t.string :contact
      t.string :email
      t.string :city
      t.string :postcode
      t.string :phone
      t.string :mobile
      t.string :fax
      t.string :npwp
      t.text :npwp_address
      t.integer :paymentdays
      t.text :description

      t.timestamps null: false
    end
  end
end
