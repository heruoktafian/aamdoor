Rails.application.routes.draw do
  devise_for :users
  get 'hitungsekaligus/index'
  get 'hrgpotong/index'
  get 'totalwaktu/index'
  get 'hrghandling/index'
  get 'dischandling/index'
  get 'berat_material/index'
  get 'keliling/index'
  get 'keliling/bulat'
  get 'keliling/square'
  get 'profile/index'
  get 'default/index'
  resources :customers
  resources :users
  resources :profile
  root :to => 'default#index'
end
